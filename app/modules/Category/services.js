// This one will get categories from cache
const redisCache = require('../../services/RedisCache')
const { promisify } = require('util')
const hgetallPromised = promisify(redisCache.redisClient.hgetall).bind(redisCache.redisClient)

const getCategoriesByIdsFromCache = async (ids, { categories: categoryMapping }) => {
    const categories = ids ? await Promise.all(Array.prototype.map.call(ids, async id => {
        const { id: catId, parent_id, title, description } = await hgetallPromised(`category[${id}]`)
        return { id: catId, parent_id, title, description }
    })) : null

    // do required fields here

    // do map here
    if (categories && categoryMapping) {
        // generate new categories array with mapped values
        let categoriesWithMappings = categories
            .filter(category => category.id in categoryMapping)
            .map(category => {
                let newCategory = {}
                if (categoryMapping[category.id].user_cat_id)
                    newCategory.id = categoryMapping[category.id].user_cat_id
                if (categoryMapping[category.id].user_cat_name)
                    newCategory.title = categoryMapping[category.id].user_cat_name

                return Object.assign({}, category, newCategory)
            })

        // remove old categories from main categories
        let categoriesWithoutMappings = categories.filter(category => !(category.id in categoryMapping))

        // push new categories and return
        return [...categoriesWithoutMappings, ...categoriesWithMappings]
    }
    return categories;
}

module.exports = { getCategoriesByIdsFromCache }