const CategoryModel = require('./model')
const AppError = require('../../helpers/AppError')

class CategoryController {
    static async index(req, res) {
        const categories = await CategoryModel.all(req.query)
        const totalCategories = await CategoryModel.count()
        const perPage = req.query.limit || 10
        res.json({
            'current_page': req.query.page || 1,
            'reesource_name': 'categories',
            'data': categories,
            'total': totalCategories,
            'per_page': perPage
        })
    }

    static async show(req, res) {
        let category = await CategoryModel.find(req.params.id, req.query.fields || '*')
        if (!category) throw new AppError('category Not Found', 404)
        res.json({ category })
    }
}

module.exports = CategoryController