const categoryRouter = require('express').Router();
const CategoryController = require('./controller')
const catchAsync = require('../../helpers/catchAsync')

// /categories
categoryRouter.get('/', catchAsync(CategoryController.index))

// /categories/id
categoryRouter.get('/:id', catchAsync(CategoryController.show))

module.exports = categoryRouter;
