const Model = require('../_Base/model')
const db = require('../../database')


class CategoryModel extends Model {
    constructor(_db) {
        super(_db)
        this.tableName = 'categories'
        this.searchableFields = ['title']
        this.jsonFields = []
    }
}

module.exports = new CategoryModel(db)
