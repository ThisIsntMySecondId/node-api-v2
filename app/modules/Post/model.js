const Model = require('../_Base/model')
const db = require('../../database')


class PostModel extends Model {
    constructor(_db) {
        super(_db)
        this.tableName = 'posts'
        this.searchableFields = ['title', 'slug', 'content']
        this.jsonFields = ['tags', 'categories', 'tagged_users']
    }

    // May this should come from action resource 
    async actions(id) {
        return await this.db('actions')
            .select()
            .where('target_id', id)
            .andWhere('target_type', 'posts')
    }

    async postFromPage(pageId) {
        return await this.db('posts')
            .where('page_id', pageId)
    }
}

module.exports = new PostModel(db)
