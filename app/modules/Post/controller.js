const PostModel = require('./model')
const AppError = require('../../helpers/AppError')
const PostServices = require('./services')

class PostController {
    static async index(req, res) {
        // These could go under one function call or you could change db values to make it look much better
        const {
            resourceFieldConfig: { post: postFieldConfigs },
            mappings: fieldMappings,
            responseFormate: responseFormate,
            replaceWords: wordReplacements
        } = req.apiUser.user_settings

        const requiredPostFields = (postFieldConfigs && Array.isArray(postFieldConfigs) && postFieldConfigs.length && ['id', ...postFieldConfigs]) || '*'

        // # For Post
        // ✅ get all posts
        // ✅ expand all posts
        // ✅ ➡ map categories and tags
        // ✅ tansform post if required
        // ✅ replace words
        // ✅ send post

        // or may be you can operate on single post and combine these function calls into one big function call and then you can use that function in single post also
        // You can do this because there seems no other stuffs done apart from operating on post i.e. as we are only using single module (post)
        // we can do as above
        let { posts, total } = await PostServices.getPostsWithTotal(Object.assign({}, req.query, { fields: requiredPostFields }))
        posts = await PostServices.expandFieldsInPostsArray(posts, fieldMappings)
        posts = PostServices.transformPosts(posts, responseFormate)
        posts = PostServices.replacedWordsInPosts(posts, wordReplacements)

        /* TODO: There have to be a general response giving service. 
                 Which can be customizable by either this controller or by error n if not then will give default response
        */
        res.json({
            'current_page': req.query.page || 1,
            'reesource_name': 'posts',
            'total': total,
            'per_page': req.query.limit || 10,
            'data': posts,
        })
    }

    static async show(req, res) {
        const {
            resourceFieldConfig: { post: postFieldConfigs },
            mappings: fieldMappings,
            responseFormate: responseFormate,
            replaceWords: wordReplacements
        } = req.apiUser.user_settings

        const requiredPostFields = (postFieldConfigs && Array.isArray(postFieldConfigs) && postFieldConfigs.length && ['id', ...postFieldConfigs]) || '*'

        let post = await PostModel.find(req.params.id, requiredPostFields)
        if (!post) throw new AppError('Post Not Found', 404)
        post = await PostServices.expandFieldsInSinglePost(post, fieldMappings)
        post = PostServices.transformPosts(post, responseFormate)
        post = PostServices.replacedWordsInPosts(post, wordReplacements)

        res.json({ ...post })
    }

    static async actions(req, res) {
        let postActions = await PostModel.actions(req.params.id)
        if (!postActions.length) throw new AppError('Post Actions Not Found', 404)
        res.json({ data: postActions })
    }
}

module.exports = PostController