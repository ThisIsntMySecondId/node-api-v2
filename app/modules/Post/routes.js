const postRouter = require('express').Router();
const PostController = require('./controller')
const catchAsync = require('../../helpers/catchAsync')

// /posts
postRouter.get('/', catchAsync(PostController.index))

// /posts/id
postRouter.get('/:id', catchAsync(PostController.show))

// /posts/id/actions
postRouter.get('/:id/actions', catchAsync(PostController.actions))

module.exports = postRouter;
