const ST = require('stjs')

const PostModel = require('./model')

const CategoryService = require('../Category/services')
const TagService = require('../Tag/services')
const UserService = require('../User/services')

// Arrow functions do not have argumeents object
async function getPostsWithTotal({ search, fields, page, limit, sort }) {
    let [posts, total] = await Promise.all([PostModel.all(arguments[0]), PostModel.count()])
    return { posts, total }
}

const expandFieldsInPostsArray = async (posts, mappings) => {
    return await Promise.all(Array.prototype.map.call(posts, async post => await expandFieldsInSinglePost(post, mappings)))
}

const expandFieldsInSinglePost = async (post, mappings) => {
    let expandableFields = {}

    if ('categories' in post)
        expandableFields['categories'] = await CategoryService.getCategoriesByIdsFromCache(post.categories, mappings)
    if ('tags' in post)
        expandableFields['tags'] = await TagService.getTagsByIdsFromCache(post.tags, mappings)
    if ('tagged_users' in post)
        expandableFields['tagged_users'] = await UserService.getUsersByIds(post.tagged_users, mappings)

    return Object.assign({}, post, expandableFields)
}

// will work for both array and object
const transformPosts = (posts, templateName) => {
    if (!['string', 'undefined'].includes(typeof templateName)) return new Error('templateName must be string')
    if (!templateName) return posts;
    let template = require(`../../Resposes/${templateName}/PostResponse.json`)
    if (!Array.isArray(posts)) return transformSinglePost(posts, template)
    return posts.map(post => transformSinglePost(post, template))
}

const transformSinglePost = (post, template) => {
    return ST.select(post).transformWith(template).root()
}

// will work for both array and object
const replacedWordsInPosts = (posts, replacements) => {
    let postsInString = JSON.stringify(posts)
    for (let key in replacements) {
        let replaceWordInRegex = new RegExp(key, 'g')
        postsInString = postsInString.replace(replaceWordInRegex, replacements[key])
    }
    return JSON.parse(postsInString)
}

const postFromPage = async (pageId) => {
    return await PostModel.postFromPage(pageId)
}

module.exports = {
    getPostsWithTotal,
    expandFieldsInPostsArray,
    expandFieldsInSinglePost,
    transformPosts,
    transformSinglePost,
    replacedWordsInPosts,
    postFromPage
}