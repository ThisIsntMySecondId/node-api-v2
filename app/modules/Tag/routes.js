const tagRouter = require('express').Router();
const TagController = require('./controller')
const catchAsync = require('../../helpers/catchAsync')

// /categories
tagRouter.get('/', catchAsync(TagController.index))

// /categories/id
tagRouter.get('/:id', catchAsync(TagController.show))

module.exports = tagRouter;
