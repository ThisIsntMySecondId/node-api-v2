const Model = require('../_Base/model')
const db = require('../../database')


class TagModel extends Model {
    constructor(_db) {
        super(_db)
        this.tableName = 'tags'
        this.searchableFields = ['title']
        this.jsonFields = []
    }
}

module.exports = new TagModel(db)
