const TagModel = require('./model')
const AppError = require('../../helpers/AppError')

class TagController {
    static async index(req, res) {
        const tags = await TagModel.all(req.query)
        const totalTags = await TagModel.count()
        const perPage = req.query.limit || 10
        res.json({
            'current_page': req.query.page || 1,
            'reesource_name': 'tags',
            'data': tags,
            'total': totalTags,
            'per_page': perPage
        })
    }

    static async show(req, res) {
        let tag = await TagModel.find(req.params.id, req.query.fields)
        if (!tag) throw new AppError('Tag Not Found', 404)
        res.json({ tag })
    }
}

module.exports = TagController