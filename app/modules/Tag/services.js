// This one will get tags from cache
const redisCache = require('../../services/RedisCache')
const { promisify } = require('util')
const hgetallPromised = promisify(redisCache.redisClient.hgetall).bind(redisCache.redisClient)

const getTagsByIdsFromCache = async (ids, { tags: tagMapping }) => {
    const tags = ids ? await Promise.all(Array.prototype.map.call(ids, async id => {
        const { id: tagId, user_id, content } = await hgetallPromised(`tag[${id}]`)
        return { id: tagId, user_id, content }
    })) : null

    // do required fields here

    // do map here
    if (tags && tagMapping) {
        // generate new tags array with mapped values
        let tagsWithMappings = tags
            .filter(tag => tag.id in tagMapping)
            .map(tag => {
                let newCategory = {}
                if (tagMapping[tag.id].user_cat_id)
                    newCategory.id = tagMapping[tag.id].user_cat_id
                if (tagMapping[tag.id].user_cat_name)
                    newCategory.content = tagMapping[tag.id].user_cat_name

                return Object.assign({}, tag, newCategory)
            })

        // remove old tags from main tags
        let tagsWithoutMappings = tags.filter(tag => !(tag.id in tagMapping))

        // push new tags and return
        return [...tagsWithoutMappings, ...tagsWithMappings]
    }
    return tags;
}

module.exports = { getTagsByIdsFromCache }