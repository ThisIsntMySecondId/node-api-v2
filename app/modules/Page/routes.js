const pageRouter = require('express').Router();
const PageController = require('./controller')
const catchAsync = require('../../helpers/catchAsync')

// /posts
pageRouter.get('/', catchAsync(PageController.index))

// /posts/id
pageRouter.get('/:id', catchAsync(PageController.show))

// /posts/id/actions
pageRouter.get('/:id/actions', catchAsync(PageController.actions))


// /posts/id/pages
pageRouter.get('/:id/posts', catchAsync(PageController.posts))

module.exports = pageRouter;
