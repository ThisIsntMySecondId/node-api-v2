const ST = require('stjs')

const PageModel = require('./model')
const TagService = require('../Tag/services')
const PostService = require('../Post/services')

// Arrow functions do not have argumeents object
async function getPagesWithTotal({ search, fields, page, limit, sort }) {
    let [pages, total] = await Promise.all([PageModel.all(arguments[0]), PageModel.count()])
    return { pages, total }
}

const expandFieldsInPagesArray = async (pages, mappings) => {
    return await Promise.all(Array.prototype.map.call(pages, async page => await expandFieldsInSinglePage(page, mappings)))
}

const expandFieldsInSinglePage = async (page, mappings) => {
    let expandableFields = {}

    if ('tags' in page)
        expandableFields['tags'] = await TagService.getTagsByIdsFromCache(page.tags, mappings)

    return Object.assign({}, page, expandableFields)
}

// will work for both array and object
const transformPages = (pages, templateName) => {
    if (!['string', 'undefined'].includes(typeof templateName)) return new Error('templateName must be string')
    if (!templateName) return pages;
    let template = require(`../../Resposes/${templateName}/PageResponse.json`)
    if (!Array.isArray(pages)) return transformSinglePage(pages, template)
    return pages.map(page => transformSinglePage(page, template))
}

const transformSinglePage = (page, template) => {
    return ST.select(page).transformWith(template).root()
}

// will work for both array and object
const replacedWordsInPages = (pages, replacements) => {
    let pagesInString = JSON.stringify(pages)
    for (let key in replacements) {
        let replaceWordInRegex = new RegExp(key, 'g')
        pagesInString = pagesInString.replace(replaceWordInRegex, replacements[key])
    }
    return JSON.parse(pagesInString)
}

module.exports = {
    getPagesWithTotal,
    expandFieldsInPagesArray,
    expandFieldsInSinglePage,
    transformPages,
    transformSinglePage,
    replacedWordsInPages
}