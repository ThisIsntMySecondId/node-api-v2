const PageModel = require('./model')
const AppError = require('../../helpers/AppError')

const PageServices = require('./services')
const PostServices = require('../Post/services')
class PageController {
    static async index(req, res) {
        const {
            resourceFieldConfig: { page: pageFieldConfigs },
            mappings: fieldMappings,
            responseFormate: responseFormate,
            replaceWords: wordReplacements
        } = req.apiUser.user_settings

        const requiredPageFields = (pageFieldConfigs && Array.isArray(pageFieldConfigs) && pageFieldConfigs.length && ['id', ...pageFieldConfigs]) || '*'

        // # For Page
        // ✅ get all pages
        // ✅ expand all pages
        // ✅ ➡ map categories and tags
        // ✅ tansform page if required
        // ✅ replace words
        // ✅ send page

        let { pages, total } = await PageServices.getPagesWithTotal(Object.assign({}, req.query, { fields: requiredPageFields }))
        pages = await PageServices.expandFieldsInPagesArray(pages, fieldMappings)
        pages = PageServices.transformPages(pages, responseFormate)
        pages = PageServices.replacedWordsInPages(pages, wordReplacements)

        res.json({
            'current_page': req.query.page || 1,
            'reesource_name': 'pages',
            'data': pages,
            'total': total,
            'per_page1': req.query.limit || 10,
        })
    }

    static async show(req, res) {
        const {
            resourceFieldConfig: { page: pageFieldConfigs },
            mappings: fieldMappings,
            responseFormate: responseFormate,
            replaceWords: wordReplacements
        } = req.apiUser.user_settings

        const requiredPageFields = (pageFieldConfigs && Array.isArray(pageFieldConfigs) && pageFieldConfigs.length && ['id', ...pageFieldConfigs]) || '*'

        let page = await PageModel.find(req.params.id, requiredPageFields)
        if (!page) throw new AppError('Page Not Found', 404)
        page = await PageServices.expandFieldsInSinglePage(page, fieldMappings)
        page = PageServices.transformPages(page, responseFormate)
        page = PageServices.replacedWordsInPages(page, wordReplacements)

        res.json({ ...page })
    }

    static async actions(req, res) {
        let pageActions = await PageModel.actions(req.params.id)
        if (!pageActions.length) throw new AppError('Page Actions Not Found', 404)
        res.json({ data: pageActions })
    }

    static async posts(req, res) {
        res.json({
            'pageId': req.params.id,
            'resourceType': 'posts',
            'data': await PostServices.postFromPage(req.params.id)
        })
    }
}

module.exports = PageController