const Model = require('../_Base/model')
const db = require('../../database')


class PageModel extends Model {
    constructor(_db) {
        super(_db)
        this.tableName = 'pages'
        this.searchableFields = ['title', 'slug']
        this.jsonFields = ['tags']
    }

    // May this should come from action resource 
    async actions(id) {
        return this.db('actions')
            .select()
            .where('target_id', id)
            .andWhere('target_type', 'pages')
    }
}

module.exports = new PageModel(db)
