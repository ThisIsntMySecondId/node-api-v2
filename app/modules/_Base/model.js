const DEFAULT_LIMIT = 10;

class BaseModel {
    constructor(_db) {
        this.db = _db
    }

    jsonParseFields(obj, fields) {
        fields.forEach(field => {
            if (field in obj) obj[field] = JSON.parse(obj[field])
        })
        return obj;
    }

    resolveJsonColumns(list) {
        return list.map(obj => {
            this.jsonFields.forEach(field => {
                if (field in obj) obj[field] = JSON.parse(obj[field])
            })
            return obj
        })
    }

    async count() {
        return (await this.db(this.tableName).count('*', { as: 'count' }))[0].count;
    }

    async all({ search = '', fields = '*', page = 1, limit = DEFAULT_LIMIT, sort = 'id' }) {
        const offset = (page && page > 0 ? page - 1 : 0) * limit
        const sortOrder = sort[0] === '-' ? [sort.slice(1), 'desc'] : [sort, 'asc']
        const requiredFields = typeof fields === 'string' ?
            fields.split(',').map(field => field.trim()) :
            fields

        return await this.db(this.tableName)
            .select(requiredFields)
            .limit(limit)
            .offset(offset)
            .where(builder => {
                if (search) {
                    this.searchableFields.forEach(fieldName => {
                        builder.orWhere(fieldName, 'like', `%${search}%`)
                    })
                }
            })
            // filters not working atm
            // .where(builder => {
            //     filteringConditions.forEach(condition => {
            //         builder.where(...condition)
            //     });
            // })
            .orderBy(...sortOrder)
            .then(this.resolveJsonColumns.bind(this));
    }

    async find(id, fields = '*') {
        const requiredFields = typeof fields === 'string' ?
            fields.split(',').map(field => field.trim()) :
            fields
        const results = (await this.db(this.tableName)
            .select(requiredFields)
            .where(builder => {
                if (Array.isArray(id)) builder.whereIn('id', id)
                else builder.where('id', id)
            })
            .then(this.resolveJsonColumns.bind(this)))

        if (Array.isArray(id)) return results
        else return results[0]
    }
}

module.exports = BaseModel;
