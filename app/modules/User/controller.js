const UserModel = require('./model')
const AppError = require('../../helpers/AppError')

class UserController {
    static async index(req, res) {
        const users = await UserModel.all(req.query)
        const totalUsers = await UserModel.count()
        const perPage = req.query.limit || 10
        res.json({
            'current_page': req.query.page || 1,
            'reesource_name': 'users',
            'data': users,
            'total': totalUsers,
            'per_page': perPage
        })
    }

    static async show(req, res) {
        let user = await UserModel.find(Object.assign({}, { id: req.params.id, fields: req.query.fields }))
        if (!user) throw new AppError('User Not Found', 404)
        res.json({ user })
    }

    static async actions(req, res) {
        let userActions = await UserModel.actions(req.params.id)
        if (!userActions.length) throw new AppError('User Actions Not Found', 404)
        res.json({ data: userActions })
    }

    static userResources(resourceName) {
        return async (req, res) => {
            return res.json({
                'userId': req.params.id,
                'resourceType': resourceName,
                'data': await UserModel.userResource(req.params.id, resourceName)
            })
        }
    }

    static async followers(req, res) {
        return res.json({
            'userId': req.params.id,
            'resourceType': 'users',
            'data': await UserModel.userFollowers(req.params.id)
        })
    }
    static async followings(req, res) {
        return res.json({
            'userId': req.params.id,
            'resourceType': 'users',
            'data': await UserModel.userFollowings(req.params.id)
        })
    }
}

module.exports = UserController