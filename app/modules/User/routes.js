const userRouter = require('express').Router();
const UserController = require('./controller')
const catchAsync = require('../../helpers/catchAsync')

// /users
userRouter.get('/', catchAsync(UserController.index))

// /users/id
userRouter.get('/:id', catchAsync(UserController.show))

// /users/id/actions
userRouter.get('/:id/actions', catchAsync(UserController.actions))

// /users/id/posts
userRouter.get('/:id/posts', catchAsync(UserController.userResources('posts')))
// /users/id/pages
userRouter.get('/:id/pages', catchAsync(UserController.userResources('pages')))
// /users/id/comments
userRouter.get('/:id/comments', catchAsync(UserController.userResources('comments')))
// /users/id/tags
userRouter.get('/:id/tags', catchAsync(UserController.userResources('tags')))

// /users/id/followers
userRouter.get('/:id/followers', catchAsync(UserController.followers))
// /users/id/followings
userRouter.get('/:id/followings', catchAsync(UserController.followings))

module.exports = userRouter;
