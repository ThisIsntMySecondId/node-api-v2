// This one will get users from db
const UserModel = require('./model')

const getUsersByIds = async ids => {
    const users = ids ? await UserModel.find(ids) : null

    // do required fields here
    // do map here
    return users;
}

module.exports = { getUsersByIds }