const Model = require('../_Base/model')
const db = require('../../database')


class UserModel extends Model {
    constructor(_db) {
        super(_db)
        this.tableName = 'users'
        this.searchableFields = ['name', 'usrename', 'email', 'region']
        this.jsonFields = ['profile']
    }

    // May this should come from action resource 
    async actions(id) {
        return await this.db('actions')
            .select()
            .where('target_id', id)
            .andWhere('target_type', 'users')
    }

    async userResource(id, resourceName) {
        return await this.db(resourceName)
            .where('user_id', id)
    }

    async userFollowers(id) {
        return await this.db('actions')
            .where('action', 'follow')
            .where('target_type', 'users')
            .where('target_id', id)
    }
    async userFollowings(id) {
        return await this.db('actions')
            .where('action', 'follow')
            .where('user_id', id)
    }

}

module.exports = new UserModel(db)
