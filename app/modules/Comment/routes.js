const commentRouter = require('express').Router();
const CommentController = require('./controller')
const catchAsync = require('../../helpers/catchAsync')

// /users
commentRouter.get('/', catchAsync(CommentController.index))

// /users/id
commentRouter.get('/:id', catchAsync(CommentController.show))

// /users/id/actions
commentRouter.get('/:id/actions', catchAsync(CommentController.actions))

module.exports = commentRouter;
