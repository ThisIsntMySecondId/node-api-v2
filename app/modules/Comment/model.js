const Model = require('../_Base/model')
const db = require('../../database')


class CommentModel extends Model {
    constructor(_db) {
        super(_db)
        this.tableName = 'comments'
        this.searchableFields = ['comment']
        this.jsonFields = []
    }

    // May this should come from action resource 
    async actions(id) {
        return this.db('actions')
            .select()
            .where('target_id', id)
            .andWhere('target_type', 'comments')
    }
}

module.exports = new CommentModel(db)
