const CommentModel = require('./model')
const AppError = require('../../helpers/AppError')

class CommentController {
    static async index(req, res) {
        const comments = await CommentModel.all(req.query)
        const totalComments = await CommentModel.count()
        const perPage = req.query.limit || 10
        res.json({
            'current_page': req.query.page || 1,
            'reesource_name': 'comments',
            'data': comments,
            'total': totalComments,
            'per_page': perPage
        })
    }

    static async show(req, res) {
        console.log('CommentController -> show -> req.params.id', req.params.id)
        let comment = await CommentModel.find(req.params.id, req.query.fields || '*')
        console.log('CommentController -> show -> comment', comment)
        if (!comment) throw new AppError('Comment Not Found', 404)
        res.json({ comment })
    }

    static async actions(req, res) {
        let commentActions = await CommentModel.actions(req.params.id)
        if (!commentActions.length) throw new AppError('Comment Actions Not Found', 404)
        res.json({ data: commentActions })
    }
}

module.exports = CommentController