const express = require('express');
const cookieParser = require('cookie-parser');
const compress = require('compression');
const cors = require('cors');
const helmet = require('helmet');
const ErrorHandlers = require('./middlewares/errors')
const morgan = require('morgan')
const logger = require('./services/logger')
const rateLimitMiddleware = require('./middlewares/rateLimit')

// Get routes
const routes = require('./routes');

// Initialize Redis
const initializeRedisCache = require('./services/initializeRedisCache')
initializeRedisCache();

// Uncaught Exceptions
process.on('uncaughtException', ErrorHandlers.uncaughtExceptionsHandler);

// declare express app
const app = express();

// Use necessary middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(compress()); // gzip compression
app.use(helmet()); // secure apps by setting various HTTP headers
app.use(cors()); // enable CORS - Cross Origin Resource Sharing

app.use(rateLimitMiddleware)

// Logger Middleware
// if (env === 'development') app.use(morgan('dev'))
app.use(morgan('dev', { stream: logger.stream }));

// Use routes
app.use('/', routes);

// 404 error handler
app.all('*', ErrorHandlers.notFoundHandler)

// Global error handler
app.use(ErrorHandlers.globalHandler)

// Unhandled Rejections
process.on('unhandledRejection', ErrorHandlers.unhandledPromisesHandler);

module.exports = app;
