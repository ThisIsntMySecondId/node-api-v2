const log = require('./components/log');
const db = require('./components/db');
const app = require('./components/app');
const redis = require('./components/redis');

const config = {
  redis: { ...redis },
  app: { ...app },
  log: { ...log },
  db: { ...db },
};

module.exports = config;
