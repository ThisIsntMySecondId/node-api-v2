// Validate Env vars
require('dotenv').config()

const appConfig = {
  name: process.env.APP_NAME || 'Node Express App',
  env: process.env.NODE_ENV || 'dev',
  key: process.env.APP_KEY || '',
  debug: process.env.APP_DEBUG || 'false',
  url: process.env.APP_URL || ''
};

module.exports = appConfig;
