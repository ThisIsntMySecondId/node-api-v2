// Validate Env vars
require('dotenv').config()

const dbConfig = {
    client: process.env.DB_CLIENT || 'mysql',
    host: process.env.DB_HOST || 'localhost',
    port: parseInt(process.env.DB_PORT) || 3306,
    database: process.env.DB_DATABASE || '',
    username: process.env.DB_USERNAME || '',
    password: process.env.DB_PASSWORD || 'mysql',
    maxPooledConnections: 10,
    minPooledConnections: 2
};

module.exports = dbConfig;
