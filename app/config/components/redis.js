// Validate Env vars
require('dotenv').config()

const redisConfig = {
    host: process.env.REDIS_HOST || 'localhost',
    port: parseInt(process.env.REDIS_PASSWORD) || 6379,
    password: process.env.REDIS_PORT || 'mysql',
};

module.exports = redisConfig;
