// Validate Env vars
require('dotenv').config()
const appRoot = require('app-root-path').path
const path = require('path')

const logConfig = {
    logfile: process.env.LOG_FILE ? path.join(appRoot, process.env.LOG_FILE) : path.join(appRoot, '/logs/'),
};

module.exports = logConfig;
