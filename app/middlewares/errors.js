const status = require('statuses')
const AppError = require('../helpers/AppError')
const { env } = require('../config')['app']
const logger = require('../services/logger')


class ErrorHandlers {
    static logError(err) {
        console.log(err)
        logger.error(err)
    }

    static notFoundHandler(req, res, next) {
        next(new AppError(`Can't find ${req.originalUrl} on this server!`, 404));
    }
    static globalHandler(err, req, res, next) {
        err.statusCode = err.statusCode || 500
        err.status = err.status || status('internal server error')

        // log err
        ErrorHandlers.logError(err)

        if (env === 'development') {
            ErrorHandlers.sendErrorDev(err, res)
        } else if (env === 'production') {
            // catch or specify other possible errors here
            // Handle Db errors here
            ErrorHandlers.sendErrorProd(err, res)
        }
    }
    static unhandledPromisesHandler(err) {
        console.log(err.name, err.message);
        console.log('UNHANDLED REJECTION! 💥 Shutting down...');
        ErrorHandlers.logError(err)
        process.exit(1);
    }
    static uncaughtExceptionsHandler(err) {
        console.log(err.name, err.message);
        console.log('UNCAUGHT EXCEPTION! 💥 Shutting down...');
        ErrorHandlers.logError(err)
        process.exit(1);
    }

    static sendErrorDev(err, res) {
        return res.status(err.statusCode).json({
            status: err.status,
            error: err,
            message: err.message,
            stack: err.stack
        });
    }

    static sendErrorProd(err, res) {
        if (err.isOperational) {
            return res.status(err.statusCode).json({
                status: err.status,
                message: err.message
            });
        } else {
            console.log(err)

            return res.status(status('internal server error')).json({
                status: 'error',
                message: 'internal server error'
            });
        }
    }
}

module.exports = ErrorHandlers