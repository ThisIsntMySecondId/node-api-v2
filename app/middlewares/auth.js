const db = require('../database')
const AppError = require('../helpers/AppError')
const httpStatus = require('statuses')

const authService = require('../services/auth')

// TODO: add history to store calls
// TODO: give user ability to generate different api keys with different scopes (permissions)

module.exports = async function (req, res, next) {
    try {
        if (!('API_KEY' in req.query))
            next(new AppError('Unauthorized Request. API_KEY is required as query parmeter for authentication', httpStatus('Unauthorized')))

        req.apiUser = await authService(req.query.API_KEY)

        req.apiUser['user_settings'] = JSON.parse(req.apiUser['user_settings'])

        next()
    } catch (error) {
        next(error)
    }
}