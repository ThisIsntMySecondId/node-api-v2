const rateLimit = require('express-rate-limit');

const oneMinute = 60 * 1000
const rateLimitWindowInMinute = 1
const maxCallInOneWindow = 1000

const rateLimiter = rateLimit({
    windowMs: rateLimitWindowInMinute * oneMinute,
    max: maxCallInOneWindow,
    message: {
        status: 'error',
        message: `Do not make more then ${maxCallInOneWindow} in one minute. We will report you.`
    }
});

module.exports = rateLimiter