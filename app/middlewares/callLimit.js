const db = require('../database')
const AppError = require('../helpers/AppError')
const httpStatus = require('statuses')

module.exports = async function (req, res, next) {
    try {
        res.append('X-API-CALL-COUNT', req.apiUser.api_call_count)
        res.append('X-API-CALL-LIMIT', req.apiUser.api_call_limit)

        if (req.apiUser.api_call_limit <= req.apiUser.api_call_count)
            return next(new AppError('You have excceeded your call limit.', httpStatus('Payment Required')))

        await db('api_users').where('id', req.apiUser.id).increment('api_call_count', 1)
        next()
    } catch (error) {
        next(error)
    }
}