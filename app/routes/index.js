const router = require('express').Router();
const authMiddleware = require('../middlewares/auth')
const callLimitMiddleware = require('../middlewares/callLimit')

// set dos rate limit middleware here

// set auth middleware here
router.use('/api', authMiddleware)

// set api rate limit middleware here
router.use('/api', callLimitMiddleware)

// set module routes here
const posts = require('../modules/Post')
router.use('/api/posts', posts);

const pages = require('../modules/Page')
router.use('/api/pages', pages);

const users = require('../modules/User')
router.use('/api/users', users);

const categories = require('../modules/Category')
router.use('/api/categories', categories);

const tags = require('../modules/Tag')
router.use('/api/tags', tags);

const comments = require('../modules/Comment')
router.use('/api/comments', comments);

module.exports = router;
