const db = require('../database')
const AppError = require('../helpers/AppError')
const httpStatus = require('statuses')

module.exports = async function (apiKey) {
    const user = await db('api_users').where('api_key', '=', apiKey)
    if (!user.length) throw new AppError('Unauthorized Request. API_KEY is invalid', httpStatus('Unauthorized'))
    return user[0]
}