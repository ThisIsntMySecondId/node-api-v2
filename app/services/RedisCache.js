const redis = require('redis')
const redisConfig = require('../config').redis
const { promisify } = require('util')

class RedisCache {
    constructor({ host, port }) {
        this.redisClient = redis.createClient({ host, port })
        this.redisClient.on('connect', function () {
            console.log('Redis client connected');
        });

        this.redisClient.on('error', function (err) {
            console.log('Something went wrong ' + err);
        })
    }

    async clearCache() {
        this.redisClient.flushdb();
    }

    async set(key, value) {
        const clientSet = promisify(this.redisClient.SET).bind(this.redisClient)
        return await clientSet(key, value)
    }

    async get(key) {
        const clientGet = promisify(this.redisClient.GET).bind(this.redisClient)
        return await clientGet(key)
    }
}

module.exports = new RedisCache({
    host: redisConfig.host,
    port: redisConfig.port
})