const redisCache = require('./RedisCache')

const { flatten } = require('../helpers/flatObj')

const db = require('../database')

const initializeRedisCache = async () => {
    await redisCache.clearCache()
    await redisCache.set('iv', 'Some Initial Value')
    await loadCategories();
    await loadTags();
    await loadApiUsers();
}

const loadCategories = async () => {
    const categories = await db('categories')

    categories.forEach(category => {
        category.parent_id = category.parent_id || 'null'
        category.updated_at = category.updated_at || 'null'
        redisCache.redisClient.hmset(`category[${category.id}]`, category)
    })
}
const loadTags = async () => {
    const tags = await db('tags')

    tags.forEach(tag => {
        tag.user_id = tag.user_id || 'null'
        tag.updated_at = tag.updated_at || 'null'
        redisCache.redisClient.hmset(`tag[${tag.id}]`, tag)
    })
}
const loadApiUsers = async () => {
    const apiUsers = await db('api_users').limit(5)

    apiUsers.forEach(apiUser => {
        apiUser.user_settings = JSON.parse(apiUser.user_settings)
        for (let key in apiUser) {
            apiUser[key] = apiUser[key] || 'null'
        }
        redisCache.redisClient.hmset(`apiUser[${apiUser.id}]`, flatten(apiUser))
    })
}

module.exports = initializeRedisCache