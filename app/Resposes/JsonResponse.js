const DataTransform = require('node-json-transform').DataTransform
const AppError = require('../helpers/AppError')

class JsonResponse {
    constructor(obj, sync = true) {
        if (this.constructor == JsonResponse) {
            throw new AppError('Cant instanciate abstract')
        }
        return sync ? this.transformSync(obj) : this.transformAsync(obj)
    }

    setMap() {
        throw new AppError('Method must be implemented')
    }

    transformSync(obj) {
        this.setMap()
        var dataTransform = DataTransform(obj, this.map)
        return dataTransform.transform()
    }

    transformAsync(obj) {
        this.setMap()
        var dataTransform = DataTransform(obj, this.map)
        return dataTransform.transformAsync()

    }
}


module.exports = JsonResponse