const db = require('../config').db
const knex = require('knex')

const knexConfig = {
    client: db.client,
    connection: {
        host: db.host,
        port: db,
        database: db.database,
        user: db.username,
        password: db.password,
    },
    pool: {
        min: db.minPooledConnections,
        max: db.maxPooledConnections
    },
}

const dbInstance = knex(knexConfig);

module.exports = dbInstance;

