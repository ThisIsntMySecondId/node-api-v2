const faker = require('faker');
const amount = 1000;
const maxTags = 10;
const maxTaggedUsers = 5;
const maxCategories = 10;

const postFactory = async (knex) => {
  return {
    "user_id": (await knex('users').orderByRaw('RAND()').limit(1))[0].id || 1,
    "page_id": (await knex('pages').orderByRaw('RAND()').limit(1))[0].id || null,
    "title": faker.lorem.sentence(),
    "slug": faker.lorem.sentence(),
    "content": faker.lorem.paragraph().substring(0, 200),
    "header_img": `https://i.picsum.photos/id/${faker.random.number(1000)}/640/480.jpg`,
    "is_published": faker.random.boolean(),
    "published_at": faker.date.past(10),
    "tags": faker.random.boolean() ? null : JSON.stringify(
      (await knex('tags').orderByRaw('RAND()').limit(faker.random.number(maxTags))).map(obj => obj.id)
    ),
    "tagged_users": faker.random.boolean() ? null : JSON.stringify(
      (await knex('users').orderByRaw('RAND()').limit(faker.random.number(maxTaggedUsers))).map(obj => obj.id)
    ),
    "categories": faker.random.boolean() ? null : JSON.stringify(
      (await knex('categories').orderByRaw('RAND()').limit(faker.random.number(maxCategories))).map(obj => obj.id)
    ),
  }
};

exports.postFactory = postFactory;

exports.seed = async function (knex) {
  //   Deletes ALL existing entries and starting from 1
  await knex('posts').del()
  await knex.raw('ALTER TABLE posts AUTO_INCREMENT = 1')

  // Inserting
  let posts = await Promise.all(Array(amount).fill({}).map(async obj => await postFactory(knex)));
  if (amount <= 1000)
    await knex('posts').insert(posts);
  else {
    posts = Array(parseInt(posts.length / 1000) + 1).fill().map((_, i) => {
      return posts.slice(1000 * i, 1000 * i + 1000)
    })
    await Promise.all(Array(amount / 1000).fill().map(async (obj, index) => await knex('posts').insert(posts[index])))
  }

  console.log(`Inserted ${amount} rows in post table`)
  return true;
};
