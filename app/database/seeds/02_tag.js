const faker = require('faker');
const amount = 1000;

const tagFactory = async (knex) => {
  return {
    "user_id" : faker.random.boolean() && (await knex('users').orderByRaw('RAND()').limit(1))[0].id || null,
    "content" : faker.company.catchPhraseNoun(),
  }
};

exports.tagFactory = tagFactory;

exports.seed = async function (knex) {
    let tags = await Promise.all(Array(amount).fill({}).map(async obj => await tagFactory(knex)));
    //   Deletes ALL existing entries and starting from 1
    await knex('tags').del()
    await knex.raw('ALTER TABLE tags AUTO_INCREMENT = 1')

    // Inserting
    await knex('tags').insert(tags);
    console.log(`Inserted ${amount} rows in tag table`)
    return true;
};
