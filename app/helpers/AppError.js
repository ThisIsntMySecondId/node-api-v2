const httpStatuses = require('statuses')

class AppError extends Error {
    constructor(message, statusCode) {
        super(message)
        this.statusCode = statusCode || 500
        this.status = httpStatuses(statusCode) || 'Internal Server Error'
        this.isOperational = true

        Error.captureStackTrace(this, this.constructor)
    }
}

module.exports = AppError