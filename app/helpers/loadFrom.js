const approot = require('app-root-path')
const path = require('path')

function appPath(modulePath) {
    return require(path.join(approot.path, 'app', modulePath))
}

function rootPath(modulePath) {
    return require(path.join(approot.path, modulePath))
}

module.exports = {
    appPath,
    rootPath
}