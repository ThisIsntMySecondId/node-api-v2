const flatten = function (ob) {
    let toReturn = {}
    for (let i in ob) {
        if (!Object.prototype.hasOwnProperty.call(ob, i)) continue

        if ((typeof ob[i]) == 'object') {
            let flatObject = flatten(ob[i])
            for (let x in flatObject) {
                if (!Object.prototype.hasOwnProperty.call(flatObject, x)) continue

                toReturn[i + '.' + x] = flatObject[x]
            }
        } else {
            toReturn[i] = ob[i]
        }
    }
    return toReturn;
};

const unflatten = (data) => {
    let toReturn = {}
    for (let i in data) {
        let keys = i.split('.')
        keys.reduce(function (r, e, j) {
            return r[e] || (r[e] = isNaN(Number(keys[j + 1])) ? (keys.length - 1 == j ? data[i] : {}) : [])
        }, toReturn)
    }
    return toReturn
}

module.exports = { flatten, unflatten }