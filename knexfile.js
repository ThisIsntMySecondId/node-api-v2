const path = require('path')
const dbconfig = require('./app/config').db

module.exports = {
  development: {
    client: dbconfig.client,
    connection: {
      host: dbconfig.host,
      port: dbconfig.port,
      database: dbconfig.database,
      user: dbconfig.username,
      password: dbconfig.password,
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: path.join(__dirname, '/app/database/migrations')
    },
    seeds: {
      directory: path.join(__dirname, '/app/database/seeds')
    }
  }
};
